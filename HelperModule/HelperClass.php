<?php
namespace App\HelperModule;


class HelperClass
{
    public static function JsonResponse($message = null, $data= null, $status=null){
        $result['message'] = $message;
        $result['status'] = $status;
        $result['data'] = $data;
        return collect($result);
    }
}
