<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
     protected $fillable = [
     'category_name', 'parent_category', 'category_descripition' , 'category_image'
    ];

     protected $table = "category";
     public function products(){
         return $this->belongsToMany('App\Products', 'productcategory', 'category_id', 'product_id');
     }
     public function getParentCategory(){
         return category::find($this->parent_category)->category_name;
     }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function childrenCategories()
    {
        return $this->hasMany(Category::class)->with('categories');
    }

}




