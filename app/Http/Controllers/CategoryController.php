<?php

namespace App\Http\Controllers;
use App\Category;
use App\HelperModule\HelperClass;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = category::latest()->paginate(5);
//        dd($data);
        return view('category.index', compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = category::all();

         return view('category.create')->with('categories', $categories);
    }

    public function parentChildCategoriesData(){
//        $parent_categories = Category::whereNull('parent_category')->get();
//        $child_categories = Category::whereNotNull('parent_category')->get();
//        $nested_array = [];
//        foreach ($parent_categories as $p_index => $category){
//            $nested_array[$p_index]['id'] = $category->id;
//            $nested_array[$p_index]['title'] = $category->category_name;
//            foreach($category->childCategory as $key => $child){
//                echo $child->id;
//                echo"<br>";
//            }
//        }
        $categories = Category::whereNull('category_id')
            ->with('childrenCategories')
            ->get();
        $result_Array = [];
        foreach ($categories as $index => $category){
            $result_Array[$index]['id'] = $category->id;
            $result_Array[$index]['text'] = $category->category_name;
            $result_Array[$index]['expanded'] = true;
            if(count($category->childrenCategories) > 0){
                foreach ($category->childrenCategories as $key => $child){
                    $result_Array[$index]['items'][$key]['id'] = $child->id;
                    $result_Array[$index]['items'][$key]['text'] = $child->category_name;
                    $result_Array[$index]['items'][$key]['expanded'] = true;
                    if(count($child->categories) > 0 ){
                        foreach ($child->categories as $subIndex => $subChild){
                            $result_Array[$index]['items'][$key]['items']['id'] = $subChild->id;
                            $result_Array[$index]['items'][$key]['items']['text'] = $subChild->category_name;
                            $result_Array[$index]['items'][$key]['items']['expanded'] = true;
                        }
                    }
                }
            }
        }
//        dd($result_Array);
        return response()->json(['message'=>'Categories list','data'=>$result_Array,'status'=>200]);
//        return HelperClass::JsonResponse('Categories list',$result_Array,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_name'    =>  'required',
            'category_descripition'     =>  'required',
//            'parent_category'     =>  'required',
        ]);
        $form_data = array(
            'category_name' => $request->category_name,
            'category_descripition' => $request->category_descripition,
        );
        if($request->parent_category == null){
            $form_data['parent_category'] = null;
            $form_data['category_level'] = 0;
        }else{
            $form_data['parent_category'] =  $request->parent_category;
            $find_level = Category::find($request->parent_category)->first();
        }
        if ($request->file('category_image') != null) {
            $image = $request->file('category_image');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $new_name);
            $form_data['category_image'] = $new_name;
        }
        else{
            $form_data['category_image'] = '';
        }
//        dd($form_data);
        Category::create($form_data);
        return redirect('category')->with('success', 'category Added successfully.');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Category::findOrFail($id);
//        dd($data->category_descripition);
        return view('category.edit', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Category::findOrFail($id);
        return view('category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Category::findOrFail($id);
        $image_name = $data->category_image;
//        dd($image_name);
        $image = $request->file('category_image');
//        dd($image);
        if ($image != null) {
            $request->validate([
                'category_name' => 'required',
                'parent_category' => 'required',
                'category_descripition' => 'required',
                'category_image' => 'image|max:2048'
            ]);

            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
        } else {
            $request->validate([
                'category_name' => 'required',
                'parent_category' => 'required',
                'category_descripition' => 'required',
            ]);
        }

        $form_data = array(
            'category_name' => $request->category_name,
            'parent_category' => $request->parent_category,
            'category_descripition' => $request->category_descripition,
            'category_image' => $image_name
        );

        Category::whereId($id)->update($form_data);
        return redirect('category')->with('success', 'category is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Category::findOrFail($id);
        $data->delete();
        return redirect('category')->with('success', 'Category is successfully deleted');
    }
}
