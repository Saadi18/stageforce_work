<?php

namespace App\Http\Controllers;
//use App\Location;
use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Location::latest()->paginate(5);

        return view('location.index', compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('location.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'location_name'    =>  'required',
            'location_description'     =>  'required',
            'street_address'    =>  'required',
            'city'    =>  'required',
            'state'    =>  'required',
            'zip_code'     =>  'required',
            'home_status'    =>  'required',
            'access_code'     =>  'required',
            'data_of_stage'    =>  'required',
            'date_of_destage'     =>  'required',
            'location_square_feet'    =>  'required',
            'price_estimate'     =>  'required',
        ]);
        $form_data = array(
            'location_name' => $request->location_name,
            'location_description' => $request->location_description,
            'street_address' => $request->street_address,
            'city' => $request->city,
            'state' => $request->state,
            'zip_code' => $request->zip_code,
            'home_status' => $request->home_status,
            'access_code' => $request->access_code,
            'data_of_stage' => $request->data_of_stage,
            'date_of_destage' => $request->date_of_destage,
            'location_square_feet' => $request->location_square_feet,
            'price_estimate' => $request->price_estimate,

        );
        if ($request->file('image') != null) {
            $image = $request->file('image');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $new_name);
            $form_data['image'] = $new_name;

        }
        else{
            $form_data['image'] = '';

        }



        Location::create($form_data);
        // dd($form_data);
        return redirect('location')->with('success', 'Location Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Location::findOrFail($id);
        //dd($data->category_descripition);
        return view('location.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Location::findOrFail($id);
        return view('location.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Location::findOrFail($id);
        $image_name = $data->image;
//        dd($image_name);
        $image = $request->file('image');
//        dd($image);
        if ($image != null) {
            $request->validate([
                'location_name'    =>  'required',
                'location_description'     =>  'required',
                'product'    =>  'required',
                'street_address'    =>  'required',
                'city'    =>  'required',
                'state'    =>  'required',
                'zip_code'     =>  'required',
                'home_status'    =>  'required',
                'access_code'     =>  'required',
                'data_of_stage'    =>  'required',
                'date_of_destage'     =>  'required',
                'location_square_feet'    =>  'required',
                'price_estimate'     =>  'required',
                'image'         =>  'image|max:2048'
            ]);

            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
        }
        else
        {
            $request->validate([
                'location_name'    =>  'required',
                'location_description'    =>  'required',
                'product'     =>  'required',
                'street_address'     =>  'required',
                'city'     =>  'required',
                'state'     =>  'required',
                'zip_code'    =>  'required',
                'home_status'     =>  'required',
                'access_code'    =>  'required',
                'data_of_stage'     =>  'required',
                'date_of_destage'     =>  'required',
                'location_square_feet'    =>  'required',
                'price_estimate'     =>  'required'

            ]);
        }

        $form_data = array(
            'location_name'       =>   $request->location_name,
            'location_description'        =>   $request->location_description,
            'product'       =>   $request->product,
            'street_address'       =>   $request->street_address,
            'city'       =>   $request->city,
            'state'       =>   $request->state,
            'zip_code'        =>   $request->zip_code,
            'home_status'       =>   $request->home_status,
            'access_code'        =>   $request->access_code,
            'data_of_stage'       =>   $request->data_of_stage,
            'date_of_destage'        =>   $request->date_of_destage,
            'location_square_feet'       =>   $request->location_square_feet,
            'price_estimate'     =>$request->price_estimate,
            'image'            =>   $image_name
        );

        Location::whereId($id)->update($form_data);
        return redirect('location')->with('success', 'Location is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Location::findOrFail($id);
        $data->delete();
        return redirect('location')->with('success', 'Location is successfully deleted');
    }
}
