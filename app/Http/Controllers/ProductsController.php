<?php

namespace App\Http\Controllers;
use App\Category;
use App\Location;
use App\Products;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Products::latest()->paginate(5);
        return view('products.index', compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = category::all();
        $location = Location::all();

        return view('products.create')->with('categories', $categories)->with('locations', $location);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name'    =>  'required',
            'product_description'     =>  'required',
            'sku'    =>  'required',
            'vender'    =>  'required',
            'length'     =>  'required',
            'width'    =>  'required',
            'height'     =>  'required',
            'purchase_date'    =>  'required',
            'purchase_price'     =>  'required',
            'rental_price'    =>  'required',
            'sale_price'     =>  'required',
        ]);
        $form_data = array(
            'product_name' => $request->product_name,
            'product_description' => $request->product_description,
            'sku' => $request->sku,
            'vender' => $request->vender,
            'length' => $request->length,
            'width' => $request->width,
            'height' => $request->height,
            'purchase_date' => $request->purchase_date,
            'purchase_price' => $request->purchase_price,
            'rental_price' => $request->rental_price,
            'sale_price' => $request->sale_price,

        );
        if ($request->file('image') != null) {
            $image = $request->file('image');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $new_name);
            $form_data['image'] = $new_name;

        }
        else{
            $form_data['image'] = '';

        }

        $prod = Products::create($form_data);

        foreach($request->category as $cat){
            $category = category::find($cat);
            $prod->categories()->save($category);
        }

        return redirect('products')->with('success', 'Product Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $data = Products::findOrFail($id);
      //dd($data->category_descripition);
        return view('products.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Products::findOrFail($id);
        return view('products.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Products::findOrFail($id);
        $image_name = $data->image;
//        dd($image_name);
        $image = $request->file('image');
//        dd($image);
        if ($image != null) {
            $request->validate([
                'product_name'    =>  'required',
                'product_description'     =>  'required',
                'category'    =>  'required',
                'sku'    =>  'required',
                'vender'    =>  'required',
                'length'     =>  'required',
                'width'    =>  'required',
                'height'     =>  'required',
                'purchase_date'    =>  'required',
                'purchase_price'     =>  'required',
                'rental_price'    =>  'required',
                'sale_price'     =>  'required',
                'image'         =>  'image|max:2048'
            ]);

            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
        }
        else
        {
            $request->validate([
                'product_name'    =>  'required',
                'product_description'    =>  'required',
                'category'     =>  'required',
                'sku'     =>  'required',
                'vender'     =>  'required',
                'length'    =>  'required',
                'width'     =>  'required',
                'height'    =>  'required',
                'purchase_date'     =>  'required',
                'purchase_price'    =>  'required',
                'rental_price'     =>  'required',
                'sale_price'     =>  'required'
            ]);
        }

        $form_data = array(
            'product_name'       =>   $request->product_name,
            'product_description'        =>   $request->product_description,
            'category'       =>   $request->category,
            'sku'       =>   $request->sku,
            'vender'       =>   $request->vender,
            'length'        =>   $request->length,
            'width'       =>   $request->width,
            'height'        =>   $request->height,
            'purchase_date'       =>   $request->purchase_date,
            'purchase_price'        =>   $request->purchase_price,
            'rental_price'       =>   $request->rental_price,
            'sale_price'     =>$request->sale_price,
            'image'            =>   $image_name
        );

        Products::whereId($id)->update($form_data);
        return redirect('products')->with('success', 'Product is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Products::findOrFail($id);
        $data->delete();
        return redirect('products')->with('success', 'Product is successfully deleted');
    }
}
