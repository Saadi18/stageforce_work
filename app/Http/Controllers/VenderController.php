<?php

namespace App\Http\Controllers;

use App\Vender;
use Illuminate\Http\Request;

class VenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = Vender::latest()->paginate(5);


        return view('vender.index', compact('data'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vender.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'vender_name'    =>  'required',
            'vender_descripition'     =>  'required',
        ]);
        $form_data = array(
            'vender_name' => $request->vender_name,
            'vender_descripition' => $request->vender_descripition,
        );
        if ($request->file('vender_image') != null) {
            $image = $request->file('vender_image');
            $new_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $new_name);
            $form_data['vender_image'] = $new_name;

        }
        else{
            $form_data['vender_image'] = '';

        }
        Vender::create($form_data);
        return redirect('vender')->with('success', 'Vender Added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Vender::findOrFail($id);
        return view('vender.view', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Vender::findOrFail($id);
//        dd($data->vender_descripition);
        return view('vender.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $data = Vender::findOrFail($id);
        $image_name = $data->vender_image;
//        dd($image_name);
        $image = $request->file('vender_image');
//        dd($image);
        if ($image != null) {
            $request->validate([
                'vender_name' => 'required',
                'vender_descripition' => 'required',
                'vender_image' => 'image|max:2048'
            ]);

            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
        } else {
            $request->validate([
                'vender_name' => 'required',
                'vender_descripition' => 'required'
            ]);
        }

        $form_data = array(
            'vender_name' => $request->vender_name,
            'vender_descripition' => $request->vender_descripition,
            'vender_image' => $image_name
        );

        Vender::whereId($id)->update($form_data);

        return redirect('vender')->with('success', 'Vender is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Vender::findOrFail($id);
        $data->delete();

        return redirect('vender')->with('success', 'Vender is successfully deleted');
    }
}
