<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
     'product_name', 'product_description','sku','category', 'vender', 'length', 'width', 'height', 'purchase_date', 'purchase_price' , 'rental_price' , 'sale_price' , 'image'
    ];

    public function categories(){
        return $this->belongsToMany('App\Category', 'productcategory', 'product_id', 'category_id');
    }

    public function categoriesintext(){
        $cats = $this->categories;
        $str = '';
        foreach ($cats as $cat){
            $str = $str."".$cat->category_name.", ";
        }
        return $str;
    }
}
