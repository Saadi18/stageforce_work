<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vender extends Model
{
     protected $fillable = [
     'vender_name', 'vender_descripition', 'vender_image'
    ];



     protected $table="vender";
}
