<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('product_name');
            $table->string('product_description');
            $table->string('category');
            $table->string('sku');
            $table->string('vender');
            $table->double('length', 20, 10);
            $table->double('width', 20, 10);
            $table->double('height', 20, 10);
            $table->dateTime('purchase_date');
            $table->double('purchase_price', 20, 10);
            $table->double('rental_price', 20, 10);
            $table->double('sale_price', 20, 10);
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
