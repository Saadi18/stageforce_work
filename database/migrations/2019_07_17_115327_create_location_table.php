<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('location_name');
            $table->string('location_description');
            $table->string('product');
            $table->string('street_address');
            $table->string('city');
            $table->string('state');
            $table->integer('zip_code');
            $table->string('home_status');
            $table->string('access_code');
            $table->dateTime('data_of_stage');
            $table->dateTime('date_of_destage');
            $table->double('location_square_feet', 20, 10);
           $table->string('price_estimate');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location');
    }
}
