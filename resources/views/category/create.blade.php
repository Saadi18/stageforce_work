@extends('layouts.admin')
@section('content_one')
    <link href="https://kendo.cdn.telerik.com/2020.1.114/styles/kendo.common-empty.min.css" rel="stylesheet-disabled" />
    <link href="https://kendo.cdn.telerik.com/2020.1.114/styles/kendo.default-v2.min.css" rel="stylesheet" />
    <link href="https://kendo.cdn.telerik.com/2020.1.114/styles/kendo.default-v2.mobile.min.css" rel="stylesheet-disabled" />
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Form Examples
                        <small>Welcome to Oreo</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Forms</a></li>
                        <li class="breadcrumb-item active">Form Examples</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Vertical</strong> Layout</h2>
                            <ul class="header-dropdown">
                                <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle"
                                                        data-toggle="dropdown" role="button" aria-haspopup="true"
                                                        aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form action="{{route('category.store')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <label for="category_name">Category Name</label>
                                <div class="form-group">
                                    <input type="text" name="category_name" id="category_name" class="form-control"
                                           placeholder="Enter your Category Name">
                                </div>


                                <label for="category_descripition">Category Descripition</label>
                                <div class="form-group">
                                    <textarea name="category_descripition" class="form-control rounded-0"
                                              id="category_descripition" rows="10"
                                              placeholder="Enter your Category Descripition"></textarea>
                                </div>
                                <label for="parent_category">Parent Category</label>
                                    <div class="form-group">
{{--                                        <input type="text" name="parent_category" id="justAnInputBox" class="form-control" placeholder="Select">--}}
                                        <input id="dropdowntree" style="width: 100%;" />
                                    </div>
                                <div class="upload">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    <input name="category_image" type="file" class="form-control" id="inputGroupFile01">
                                </div>

                                <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Add
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
@section('custom_js')
    <script src="https://kendo.cdn.telerik.com/2020.1.114/js/kendo.all.min.js"></script>
    <script>
    $(document).ready(function () {
        $.ajax({
            url: "{{route('categories_list')}}",
            type: "get",
            dataType: "json",
            success: function (result) {
                $("#dropdowntree").kendoDropDownTree({
                    placeholder: "Select ...",
                    checkboxes: true,
                    checkAll: true,
                    autoClose: false,
                    dataSource: result.data
                });
            },
            error: function (xhr, textStatus, errorThrown) {

            }
        });

    })

</script>
@endsection

