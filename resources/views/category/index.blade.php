@extends('layouts.admin')


@section('content_one')
    <section class="content ecommerce-page">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Product List
                        <small>Welcome to Oreo</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                        <li class="breadcrumb-item"><a href="ec-dashboard.html">eCommerce</a></li>
                        <li class="breadcrumb-item active">Product List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card product_item_list">
                        <div class="body table-responsive">
                            <table class="table table-hover m-b-0">
                                <thead>
                                <tr>
                                    <th>Category Image</th>
                                    <th>Category Name</th>
                                    <th data-breakpoints="xs md">Category Description</th>
                                    <th data-breakpoints="xs md">Parent Category</th>
                                    <th data-breakpoints="xs md">Category Level</th>
                                    <th data-breakpoints="xs">Created At</th>
                                    <th data-breakpoints="sm xs md">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)

                                    <tr>
                                        <td><img src="images/{{$row->category_image}}" width="48" alt="Product img"></td>
                                        <td><h5>{{$row->category_name}}</h5></td>
                                        <td><h5>{{$row->category_descripition}}</h5></td>
                                        @if($row->parent_category != null)
                                            <td><span class="text-muted">{{$row->getParentCategory()}}</span></td>
                                        @else
                                            <td><span class="text-muted">No Parent</span></td>
                                        @endif
                                        <td><span class="col-red">{{$row->category_level}}</span></td>
                                        <td><span class="col-red">{{$row->created_at}}</span></td>
                                        <td>
                                            <a href="{{route('category.edit', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-green"><i
                                                        class="zmdi zmdi-edit"></i></a>
                                            <a href="{{route('category.delete', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-red"><i
                                                        class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="body">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
