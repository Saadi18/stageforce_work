@extends('layouts.admin')


@section('content_one')
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Form Examples
                        <small>Welcome to Oreo</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Forms</a></li>
                        <li class="breadcrumb-item active">Form Examples</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Vertical</strong> Layout</h2>
                            <ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form action="{{route('location.update', $data->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <label for="location_name">Location Name</label>
                                <div class="form-group">
                                    <input type="text"  name="location_name" value="{{$data->location_name}}" id="product_name" class="form-control" placeholder="Enter your Category Name">
                                </div>
                                <label for="location_description">Location Description</label>
                                <div class="form-group">
                                    <textarea name="location_description" value="" class="form-control rounded-0" id="location_description" rows="10" placeholder="Enter your Category Descripition">{{$data->location_description}} </textarea>
                                </div>
                                <label for="product">Product</label>
                                <div class="form-group">
                                    <input type="text"  name="product" value="{{$data->product}}" id="product" class="form-control" placeholder="Enter Product">
                                </div>
                                <label for="street_address">Street Address</label>
                                <div class="form-group">
                                    <input type="text"  name="street_address" value="{{$data->street_address}}" id="sku" class="form-control" placeholder="Enter Category">
                                </div>
                                <label for="city">City</label>
                                <div class="form-group">
                                    <input type="text"  name="city" value="{{$data->city}}" id="sku" class="form-control" placeholder="Enter Sku">
                                </div>
                                <label for="state">State</label>
                                <div class="form-group">
                                    <input type="text"  name="state" value="{{$data->state}}" id="vender" class="form-control" placeholder="Enter Vender">
                                </div>
                                <label for="zip_code">Zip Code</label>
                                <div class="form-group">
                                    <input type="number"  name="zip_code"  value="{{$data->zip_code}}" id="zip_code" class="form-control" placeholder="Enter Length">
                                </div>
                                <label for="home_status">Home Status</label>
                                <div class="form-group">
                                    <input type="text"  name="home_status" value="{{$data->home_status}}" id="width" class="form-control" placeholder="Enter Width">
                                </div>
                                <label for="access_code">Access Code</label>
                                <div class="form-group">
                                    <input type="text"  name="access_code" value="{{$data->access_code}}" id="access_code" class="form-control" placeholder="Enter Height">
                                </div>
                                <label for="data_of_stage">Data of Stage</label>
                                <div class="form-group">
                                    <input type="date"  name="data_of_stage" value="{{$data->data_of_stage}}" id="data_of_stage" class="form-control" >
                                </div>
                                <label for="date_of_destage">Date of Destage</label>
                                <div class="form-group">
                                    <input type="date"  name="date_of_destage" value="{{$data->date_of_destage}}" id="date_of_destage" class="form-control" placeholder="Enter Purchase Price">
                                </div>
                                <label for="location_square_feet">Location Square Feet</label>
                                <div class="form-group">
                                    <input type="number" step="any"  name="location_square_feet" value="{{$data->location_square_feet}}" id="location_square_feet" class="form-control" placeholder="Enter Rental Price">
                                </div>
                                <label for="price_estimate">Price Estimate</label>
                                <div class="form-group">
                                    <input type="number" step="any" name="price_estimate"  value="{{$data->price_estimate}}" id="price_estimate" class="form-control" placeholder="Enter your Sale Price">
                                </div>
                                <div class="upload">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    <input name="image" type="file" class="form-control" id="inputGroupFile01" >
                                </div>

                                <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection