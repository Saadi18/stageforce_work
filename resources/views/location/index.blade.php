@extends('layouts.admin')


@section('content_one')
    <section class="content ecommerce-page">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Location List
                        <small>Welcome to Oreo</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="/home"><i class="zmdi zmdi-home"></i>Home</a></li>
                        <li class="breadcrumb-item"><a href="/location">Location</a></li>
                        <li class="breadcrumb-item active">Location List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card product_item_list">
                        <div class="body table-responsive">
                            <table class="table table-hover m-b-0">
                                <thead>
                                <tr>
                                    <th>Location Image</th>
                                    <th>Location Name</th>
                                    <th>Location Description</th>
                                    <th data-breakpoints="xs md">Product</th>
                                    <th data-breakpoints="xs md">Street Address</th>
                                    <th data-breakpoints="xs md">City</th>
                                    <th data-breakpoints="xs md">State</th>
                                    <th data-breakpoints="xs md">Zip Code</th>
                                    <th data-breakpoints="xs md">Home Status</th>
                                    <th data-breakpoints="xs md">Access Code</th>
                                    <th data-breakpoints="xs md">Data of Stage</th>
                                    <th data-breakpoints="xs md">Date of Destage</th>
                                    <th data-breakpoints="xs md">Location Square Feet</th>
                                    <th data-breakpoints="xs md">Price Estimate</th>
                                    <th data-breakpoints="xs">Created At</th>
                                    <th data-breakpoints="xs md">Updated At</th>
                                    <th data-breakpoints="sm xs md">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)
                                    <tr>
                                        <td><img src="images/{{$row->image}}" width="48" alt="Product img"></td>
                                        <td><h5>{{$row->location_name}}</h5></td>
                                        <td><h5>{{$row->location_description}}</h5></td>
                                        <td><h5>{{$row->product}}</h5></td>
                                        <td><h5>{{$row->street_address}}</h5></td>
                                        <td><h5>{{$row->city}}</h5></td>
                                        <td><h5>{{$row->state}}</h5></td>
                                        <td><h5>{{$row->zip_code}}</h5></td>
                                        <td><h5>{{$row->home_status}}</h5></td>
                                        <td><h5>{{$row->access_code}}</h5></td>
                                        <td><h5>{{$row->data_of_stage}}</h5></td>
                                        <td><h5>{{$row->date_of_destage}}</h5></td>
                                        <td><h5>{{$row->location_square_feet}}</h5></td>
                                        <td><h5>{{$row->price_estimate}}</h5></td>
                                        <td><span class="col-red">{{$row->created_at}}</span></td>
                                        <td><span class="col-red">{{$row->updated_at}}</span></td>
                                        <td>
                                            <a href="{{route('location.edit', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-green"><i
                                                        class="zmdi zmdi-edit"></i></a>
                                            <a href="{{route('location.delete', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-red"><i
                                                        class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {!! $data->links() !!}
                        </div>
                    </div>
                    <div class="card">
                        <div class="body">
                            <ul class="pagination pagination-primary m-b-0">
                                <li class="page-item"><a class="page-link" href="#"><i class="zmdi zmdi-arrow-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i
                                                class="zmdi zmdi-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection