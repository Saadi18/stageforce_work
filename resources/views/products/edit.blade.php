@extends('layouts.admin')


@section('content_one')
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Form Examples
                        <small>Welcome to Oreo</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Forms</a></li>
                        <li class="breadcrumb-item active">Form Examples</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><strong>Vertical</strong> Layout</h2>
                            <ul class="header-dropdown">
                                <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                                <li class="remove">
                                    <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form action="{{route('products.update', $data->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <label for="product_name">Product Name</label>
                                <div class="form-group">
                                    <input type="text"  name="product_name" value="{{$data->product_name}}" id="product_name" class="form-control" placeholder="Enter your Category Name">
                                </div>
                                <label for="product_description">Product Description</label>
                                <div class="form-group">
                                    <textarea name="product_description" valu="" class="form-control rounded-0" id="product_description" rows="10" placeholder="Enter your Category Descripition">{{$data->product_description}} </textarea>
                                </div>
                                <label for="category">category</label>
                                <div class="form-group">
                                    <input type="text"  name="category" value="{{$data->category}}" id="sku" class="form-control" placeholder="Enter Category">
                                </div>
                                <label for="sku">SKU</label>
                                <div class="form-group">
                                    <input type="text"  name="sku" value="{{$data->sku}}" id="sku" class="form-control" placeholder="Enter Sku">
                                </div>
                                <label for="vender">Select Vender</label>
                                <div class="form-group">
                                    <input type="text"  name="vender" value="{{$data->vender}}" id="vender" class="form-control" placeholder="Enter Vender">
                                </div>
                                <label for="length">Length</label>
                                <div class="form-group">
                                    <input type="number"  name="length"  value="{{$data->length}}" id="length" class="form-control" placeholder="Enter Length">
                                </div>
                                <label for="width">Width</label>
                                <div class="form-group">
                                    <input type="number"  name="width" value="{{$data->width}}" id="width" class="form-control" placeholder="Enter Width">
                                </div>
                                <label for="height">Height</label>
                                <div class="form-group">
                                    <input type="number"  name="height" value="{{$data->height}}" id="height" class="form-control" placeholder="Enter Height">
                                </div>
                                <label for="purchase_date">Purchase Date</label>
                                <div class="form-group">
                                    <input type="date"  name="purchase_date" value="{{$data->purchase_date}}" id="purchase_date" class="form-control" >
                                </div>
                                <label for="purchase_price">Purchase Price</label>
                                <div class="form-group">
                                    <input type="number"  name="purchase_price" value="{{$data->purchase_price}}" id="purchase_price" class="form-control" placeholder="Enter Purchase Price">
                                </div>
                                <label for="rental_price">Rental Price</label>
                                <div class="form-group">
                                    <input type="number"  name="rental_price" value="{{$data->rental_price}}" id="rental_price" class="form-control" placeholder="Enter Rental Price">
                                </div>
                                <label for="sale_price">Sale Price</label>
                                <div class="form-group">
                                    <input type="text"  name="sale_price"  value="{{$data->sale_price}}" id="sale_price" class="form-control" placeholder="Enter your Sale Price">
                                </div>
                                <div class="upload">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                    <input name="image" type="file" class="form-control" id="inputGroupFile01" >
                                </div>

                                <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Add</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection