@extends('layouts.admin')


@section('content_one')
    <section class="content ecommerce-page">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Product List
                        <small>Welcome to Oreo</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                        <li class="breadcrumb-item"><a href="ec-dashboard.html">eCommerce</a></li>
                        <li class="breadcrumb-item active">Product List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card product_item_list">
                        <div class="body table-responsive">
                            <table class="table table-hover m-b-0">
                                <thead>
                                <tr>
                                    <th>Product Image</th>
                                    <th>Product Name</th>
                                    <th>Product Description</th>
                                    <th data-breakpoints="xs md">Category</th>
                                    <th data-breakpoints="xs md">SKU</th>
                                    <th data-breakpoints="xs md">Vender</th>
                                    <th data-breakpoints="xs md">Length</th>
                                    <th data-breakpoints="xs md">Width</th>
                                    <th data-breakpoints="xs md">Height</th>
                                    <th data-breakpoints="xs md">Purchase Date</th>
                                    <th data-breakpoints="xs md">Purchase Price</th>
                                    <th data-breakpoints="xs md">Rental Price</th>
                                    <th data-breakpoints="xs md">Sale Price</th>
                                    <th data-breakpoints="xs">Created At</th>
                                    <th data-breakpoints="xs md">Updated At</th>
                                    <th data-breakpoints="sm xs md">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)
                                    <tr>
                                        <td><img src="images/{{$row->image}}" width="48" alt="Product img"></td>
                                        <td><h5>{{$row->product_name}}</h5></td>
                                        <td><h5>{{$row->product_description}}</h5></td>
                                        <td><h5>{{$row->categoriesintext()}}</h5></td>
                                        <td><h5>{{$row->sku}}</h5></td>
                                        <td><h5>{{$row->vender}}</h5></td>
                                        <td><h5>{{$row->length}}</h5></td>
                                        <td><h5>{{$row->width}}</h5></td>
                                        <td><h5>{{$row->height}}</h5></td>
                                        <td><h5>{{$row->purchase_date}}</h5></td>
                                        <td><h5>{{$row->purchase_price}}</h5></td>
                                        <td><h5>{{$row->rental_price}}</h5></td>
                                        <td><h5>{{$row->sale_price}}</h5></td>
                                        <td><span class="col-red">{{$row->created_at}}</span></td>
                                        <td><span class="col-red">{{$row->updated_at}}</span></td>
                                        <td>
                                            <a href="{{route('products.edit', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-green"><i
                                                        class="zmdi zmdi-edit"></i></a>
                                            <a href="{{route('products.delete', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-red"><i
                                                        class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            {!! $data->links() !!}
                        </div>
                    </div>
                    <div class="card">
                        <div class="body">
                            <ul class="pagination pagination-primary m-b-0">
                                <li class="page-item"><a class="page-link" href="#"><i class="zmdi zmdi-arrow-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i
                                                class="zmdi zmdi-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection