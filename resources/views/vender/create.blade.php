@extends('layouts.admin')


@section('content_one')
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Form Examples
                    <small>Welcome to Oreo</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                    <li class="breadcrumb-item"><a href="javascript:void(0);">Forms</a></li>
                    <li class="breadcrumb-item active">Form Examples</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <!-- Vertical Layout -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>Vender</strong>Form</h2>
                        <ul class="header-dropdown">
                            <li class="dropdown"> <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <i class="zmdi zmdi-more"></i> </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else</a></li>
                                </ul>
                            </li>
                            <li class="remove">
                                <a role="button" class="boxs-close"><i class="zmdi zmdi-close"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <form action="{{route('vender.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <label for="email_address">Vender Name</label>
                            <div class="form-group">
                                <input type="text"  name="vender_name" id="vender_name" class="form-control" placeholder="Enter your Vender Name">
                            </div>
                            <label for="email_address">Vender Descripition</label>
                            <div class="form-group">
                                <textarea name="vender_descripition" class="form-control rounded-0" id="exampleFormControlTextarea1" rows="10" placeholder="Enter your vender_descripition"></textarea>
                            </div>
                            <div class="upload">
                                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                <input name="vender_image" type="file" class="form-control" id="inputGroupFile01" >
                            </div>

                            <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>
@endsection