@extends('layouts.admin')


@section('content_one')
    <section class="content ecommerce-page">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-7 col-md-6 col-sm-12">
                    <h2>Vender's List
                        <small>here is the list of all Vender</small>
                    </h2>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-12">
                    <ul class="breadcrumb float-md-right">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i> Oreo</a></li>
                        <li class="breadcrumb-item"><a href="/vender">Vender's</a></li>
                        <li class="breadcrumb-item active">Vender List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card product_item_list">
                        <div class="body table-responsive">
                            <table class="table table-hover m-b-0">
                                <thead>
                                <tr>
                                    <th>Vender Image</th>
                                    <th>Vender Name</th>
                                    <th data-breakpoints="xs md">Vender Description</th>
                                    <th data-breakpoints="xs">Created At</th>
                                    <th data-breakpoints="xs md">Updated At</th>
                                    <th data-breakpoints="sm xs md">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $row)

                                    <tr>
                                        <td><img src="images/{{$row->vender_image}}" width="48" alt="Product img"></td>
                                        <td><h5>{{$row->vender_name}}</h5></td>
                                        <td><span class="text-muted">{{$row->vender_descripition}}</span></td>
                                        <td><span class="col-red">{{$row->created_at}}</span></td>
                                        <td><span class="col-red">{{$row->updated_at}}</span></td>
                                        <td>
                                            <a href="{{route('vender.edit', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-green"><i
                                                        class="zmdi zmdi-edit"></i></a>
                                            <a href="{{route('vender.delete', $row->id)}}"
                                               class="btn btn-default waves-effect waves-float waves-red"><i
                                                        class="zmdi zmdi-delete"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="body">
                            <ul class="pagination pagination-primary m-b-0">
                                <li class="page-item"><a class="page-link" href="#"><i class="zmdi zmdi-arrow-left"></i></a>
                                </li>
                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i
                                                class="zmdi zmdi-arrow-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection