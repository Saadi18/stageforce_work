<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::resource('vender','VenderController')->name('vender');



Route::get('/vender', 'VenderController@index')->name('vender.index');
Route::get('/vender/create', 'VenderController@create')->name('vender.create');
Route::post('/vender/store', 'VenderController@store')->name('vender.store');
Route::get('/vender/edit/{id}', 'VenderController@edit')->name('vender.edit');
Route::post('/vender/update/{id}', 'VenderController@update')->name('vender.update');
Route::get('/vender/delete/{id}', 'VenderController@destroy')->name('vender.delete');

Route::get('/category', 'CategoryController@index')->name('category.index');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::post('/category/store', 'CategoryController@store')->name('category.store');
Route::get('/category/edit/{id}', 'CategoryController@edit')->name('category.edit');
Route::post('/category/update/{id}', 'CategoryController@update')->name('category.update');
Route::get('/category/delete/{id}', 'CategoryController@destroy')->name('category.delete');
Route::get('categories_list', 'CategoryController@parentChildCategoriesData')->name('categories_list');


Route::get('/products', 'ProductsController@index')->name('products.index');
Route::get('/products/create', 'ProductsController@create')->name('products.create');
Route::post('/products/store', 'ProductsController@store')->name('products.store');
Route::get('/products/edit/{id}', 'ProductsController@edit')->name('products.edit');
Route::post('/products/update/{id}', 'ProductsController@update')->name('products.update');
Route::get('/products/delete/{id}', 'ProductsController@destroy')->name('products.delete');


Route::get('/location', 'LocationController@index')->name('location.index');
Route::get('/location/create', 'LocationController@create')->name('location.create');
Route::post('/location/store', 'LocationController@store')->name('location.store');
Route::get('/location/edit/{id}', 'LocationController@edit')->name('location.edit');
Route::post('/location/update/{id}', 'LocationController@update')->name('location.update');
Route::get('/location/delete/{id}', 'LocationController@destroy')->name('location.delete');






Route::resource('product','ProductController');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/changePassword','HomeController@showChangePasswordForm');

Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
